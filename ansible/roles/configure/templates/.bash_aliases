# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias dcud='docker-compose up -d'
alias dcd='docker-compose down'
alias deit='docker exec -it'
alias drit='docker run -it'

#!/usr/bin/env bash

#docker run -it \
#--network=host \
#-v $PWD:/ansible \
#ansible/ansible \
playbook=${1}
ansible-playbook -i inventory.yml "${playbook}"

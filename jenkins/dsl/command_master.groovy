pipelineJob("Command_Center_Master_Job") {
    description("This is my master job for the command center project")
    keepDependencies(false)
    definition {
        cpsScm {
            pipeline{
                agent{
                    label "node"
                }
                stages{
                    stage("build"){
                        steps{
                            echo "========executing Build========"
                            git credentialsId: '669f99c4-f781-4ab0-9b3b-ec22dde11102', url: 'git@gitlab.com:hasanaburayyan/my-command-center.git'
                            updateGitlabCommitStatus name: 'build', state: 'pending'
                            sh 'ls'
                            sh 'cd app/command-center && npm install && npm build'
                        }
                        post{
                            success{
                                echo "========Build executed successfully========"
                                echo "Continue Pipeline!"

                            }
                            failure{
                                echo "========Build execution failed========"
                                updateGitlabCommitStatus name: 'build', state: 'failed'
                            }
                        }
                    }
                    stage("Tag"){
                        steps {
                            echo "=========Running Tag Build========="
                            build job: 'Tag Build', parameters: [string(name: 'release', value: '0.1'), string(name: "app_version", value: "\${BUILD_NUMBER}")]
                        }
                    }
                    stage("Release Tag"){
                        steps {
                            echo "=========Running Tag Build========="
                            build job: 'Tag Release', parameters: [string(name: 'release', value: '0.1'), string(name: "app_version", value: "\${BUILD_NUMBER}"), string(name: 'tag_suffix', value: 'rc')]
                        }
                    }
                }
                post{
                    success{
                        echo "========pipeline executed successfully ========"
                        updateGitlabCommitStatus name: 'build', state: 'success'
                    }
                    failure{
                        echo "========pipeline execution failed========"
                        updateGitlabCommitStatus name: 'build', state: 'failed'
                    }
                }
            }
    	}
    }
    disabled(false)
}

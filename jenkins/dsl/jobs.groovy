job("test-1") {
	description()
	keepDependencies(false)
	disabled(false)
	concurrentBuild(false)
	steps {
		shell("""
#!/usr/bin/env bash
sleep 10
echo \"Done Yo! \${BUILD_NUMBER}\"
""")
	}
}
job("Tag Build") {
	description("Tag A Successful Build")
	keepDependencies(false)
	parameters {
		stringParam("release", "0.1", "Major . Minor Version")
		stringParam("app_version", "", "Version of application to tag")
	}
	environmentVariables {
		groovy()
		loadFilesFromMaster(false)
		keepSystemVariables(true)
		keepBuildVariables(true)
		overrideBuildParameters(false)
	}
	scm {
		git {
			remote {
				name("origin")
				url("git@gitlab.com:hasanaburayyan/my-command-center.git")
				credentials("669f99c4-f781-4ab0-9b3b-ec22dde11102")
			}
			branch("*/master")
			extensions {
				wipeOutWorkspace()
			}
		}
	}
	disabled(false)
	concurrentBuild(false)
	steps {
		shell("""
#!/usr/bin/env bash
cd scripts
build_version=\"\${release}.\${app_version}\"
git tag -d \"\${build_version}\" || echo \"No Local Tag Found, Skipping Delete...\"
git tag \"\${build_version}\"
git push origin \"\${build_version}\"
""")
	}
}

job("Tag Release") {
	description("Tag A Release Candidate Of Command Center")
	keepDependencies(false)
	parameters {
		stringParam("release", "0.1", "Major . Minor Version")
		stringParam("tag_suffix", "rc", "This is a release candidate")
	}
	environmentVariables {
		groovy()
		loadFilesFromMaster(false)
		keepSystemVariables(true)
		keepBuildVariables(true)
		overrideBuildParameters(false)
	}
	scm {
		git {
			remote {
				name("origin")
				url("git@gitlab.com:hasanaburayyan/my-command-center.git")
				credentials("669f99c4-f781-4ab0-9b3b-ec22dde11102")
			}
			branch("*/master")
			extensions {
				wipeOutWorkspace()
			}
		}
	}
	disabled(false)
	concurrentBuild(false)
	steps {
		shell("""
#!/usr/bin/env bash
cd scripts
./tag_release.sh
""")
	}
}

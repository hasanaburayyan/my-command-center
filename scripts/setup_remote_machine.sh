#!/usr/bin/env bash

SERVER=${1}

USER="hasan"

if [[ ! -f ~/.ssh/home_lab ]];
then
  echo "Generating Home Lab Key..."
  ssh-keygen -b 2048 -t rsa -f ~/.ssh/home_lab -N ""
fi

echo "Copying over ssh key"
ssh-copy-id -i ~/.ssh/home_lab ${USER}@${SERVER}

#ssh -i ~/.ssh/home_lab ${USER}@${SERVER} "echo '${USER}  ALL=(ALL) NOPASSWD:ALL' | sudo tee /etc/sudoers.d/${USER}"

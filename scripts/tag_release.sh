#!/usr/bin/env bash

version="${app_version}"

if [[ -z "${version}" ]] || [[ -z "${tag_suffix}" ]] || [[ -z "${release}" ]]
then
    echo "Mising Release, Version, Or Tag Suffix!"
    echo "Release: ${release}"
    echo "Version: ${version}"
    echo "Tag Suffix: ${tag_suffix}"
    exit 1
fi

release_version="${release}.${version}"

git tag -d "${release_version}.${tag_suffix}" || echo "No local tag ${release_version}.${tag_suffix} found, skipping delete..."
git tag "${release_version}.${tag_suffix}"
git push origin "${release_version}.${tag_suffix}"
